package sk.fiit.general;

import org.opencv.core.Scalar;

public final class Settings {
	
	public static final Scalar    	VEHICLE_TRACKING_COLOR     	= new Scalar(0, 255, 0, 255);
	public static final Scalar    	VEHICLE_NEW_COLOR     		= new Scalar(255, 255, 0, 255);
	public static final Scalar    	TEXT_COLOR					= new Scalar(255,0, 0, 255);
	public static final double 		DISTANCE_TRASHOLD 			= 0.1; 							// [%] how far can vehicle be from last known position to be considered for the same one
	public static final double 		POSITIONS_MEMORY_SIZE		= 5; 							// maximal count of memorized previous positions
	public static final int 		POSITION_IS_OLD				= 3; 							// determines when the vehicle should be deleted (not found in last N images)
	public static final int 		SPEED_REFRESH_TRASHOLD		= 7; 							// frequency of speed recalculation
	public static final double 		HORIZONTAL_FIELD_OF_VIEW	= 36.67;						// horizontal field of view specific for our camera
	public static double 			DEVICE_ANGLE				= 3.0;
	public static double 			HEIGHT_OF_CAMERA_ABOVE_ROAD = 100;
	public static int 				IMAGE_HEIGHT				= 1000;
	public static int 				FRAME_FREQUENCY				= 3;							// how often will vehicles be detected
	public static int				FRAME_TIME_MEMORY			= 15;							// how many frame capture times will be memorizes
	public static long				MAX_GPS_SIGNAL_AGE			= 5000;							// [ms] how old can GPS data be to be still considered relevant
	public static float 			GRAVITY_ACCELERATION		= 9.81f;						// g
	public static int				ANGLE_NOT_SET				= -1000;
	public static int				ANGLE_CALIBRATION_COUNT		= 10;							// how many times the angle of device will be measured
	public static double			VEHICLE_COLOR_TRASHOLD		= 0.55f;						// [0 - 255] how dark pixel must be to be considered a vehicle
	public static boolean 			TESTING_MODE				= false;						
	public static int 				TESTING_OUTPUT_FREQUENCY	= 1000;
	
	public static void setDeviceAngle(double angle) 
	{
		DEVICE_ANGLE = angle;
	}
	
	public static void setCameraHeight(double height)
	{
		HEIGHT_OF_CAMERA_ABOVE_ROAD = height;
	}

	public static void setImageHeight(int height) {
		IMAGE_HEIGHT = height;
	}
	
	


}
