package sk.fiit.vehicledetect;

import org.opencv.core.Rect;
import org.opencv.core.Scalar;

public class VehicleDrawData {
	
	private Rect position;
	private Scalar color;
	private double speed;
	private int vehicleBottom;
	
	public VehicleDrawData(Rect position, Scalar color, double speed, int vehicleBottom) {
		super();
		this.position = position;
		this.color = color;
		this.speed = speed;
		this.vehicleBottom = vehicleBottom;
	}

	public Rect getPosition() {
		return position;
	}

	public void setPosition(Rect position) {
		this.position = position;
	}

	public Scalar getColor() {
		return color;
	}

	public void setColor(Scalar color) {
		this.color = color;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}
	
	public int getVehicleBottom()
	{
		return vehicleBottom;
	}

}
