package sk.fiit.vehicledetect;

import java.util.ArrayList;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;

import sk.fiit.general.Settings;

import android.util.Log;

public class DetectedVehiclesController {
	
    private static final String TAG = "OCVSample::VehiclesController";
	
    private ArrayList<DetectedVehicle> detectedVehicles = new ArrayList<DetectedVehicle>();
	
	public void update(Rect[] newVehicles, Mat image)
	{
		// delete old vehicles
        for (int i = 0; i < detectedVehicles.size(); i++) 
    	{
        	if (detectedVehicles.get(i).isOld()) detectedVehicles.remove(i);
    	}
        
        // update existing vehicles with new positions
        for (Rect newPosition : newVehicles)
        {
        	boolean vehicleFound = false;
        	for(DetectedVehicle vehicle: detectedVehicles)
        	{
        		if (vehicle.isTheSameVehicle(newPosition, Settings.IMAGE_HEIGHT))
        		{
        			vehicle.update(newPosition, findVehicleBottom(newPosition, image), System.currentTimeMillis());
        			vehicleFound = true;
        		}
        	}
        	 
        	// insert new vehicle if no match found 
        	if ((! vehicleFound) && (! isDuplicity(newPosition)))
        	{
        		detectedVehicles.add(new DetectedVehicle(newPosition, findVehicleBottom(newPosition, image), System.currentTimeMillis()));
        	}
        }	
        Log.i(TAG, "Updated vehicles, current count: "  + Integer.toString(detectedVehicles.size()));
	}
	
	public ArrayList<VehicleDrawData> getVehicles(){
		ArrayList<VehicleDrawData> result = new ArrayList<VehicleDrawData>();
		
		for (DetectedVehicle vehicle : detectedVehicles)
		{
			Scalar color;
			color = vehicle.isNew() ? Settings.VEHICLE_NEW_COLOR : Settings.VEHICLE_TRACKING_COLOR;
			Log.i(TAG, "returning new vehicle: "  + Boolean.toString(vehicle.isNew()));
			result.add(new VehicleDrawData(vehicle.getLastKnownPosition().getPos(), color, vehicle.getSpeed(), vehicle.getLastKnownPosition().getVehicleBottom()));
		}
			
		return result;
	}
	
	private int findVehicleBottom(Rect position,Mat img)
	{
		int i = Double.valueOf(position.br().y).intValue();
		int x = Double.valueOf((position.br().x + position.tl().x) / 2).intValue();
		int res = Double.valueOf(position.br().y).intValue();
		Log.e(TAG, "bottom:"  + Integer.toString(i) + " -> " + Double.toString(position.tl().y ));
		double trashold = img.get(i, x)[0] * Settings.VEHICLE_COLOR_TRASHOLD;
		
		while (i > position.tl().y )
		{
			//Log.e(TAG, "bottom:"  + Double.toString(img.get(i, x)[0]));
			
			if (img.get(i, x)[0] < trashold /*Settings.VEHICLE_COLOR_TRASHOLD*/)
			{	
				Log.e(TAG, "bottom:"  + Integer.toString(i));
				return i;
			}
			i -= 5;
		}
		return res;
	}
	
	private boolean isDuplicity(Rect position)
	{	
		for (DetectedVehicle vehicle : detectedVehicles)
		{	
			if (vehicle.getLastKnownPosition().getPos().contains(position.br())) return true;
			if (vehicle.getLastKnownPosition().getPos().contains(position.tl())) return true;
			if (vehicle.getLastKnownPosition().getPos().contains(new Point(position.br().x, position.tl().y))) return true; //tr
			if (vehicle.getLastKnownPosition().getPos().contains(new Point(position.tl().x, position.br().y))) return true; //bl
		}	
		return false;
	}
}
