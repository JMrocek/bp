package sk.fiit.vehicledetect;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Locale;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.objdetect.CascadeClassifier;

import sk.fiit.general.Settings;
import sk.fiit.location.GPS;
import sk.fiit.position.Accelerometer;
import sk.fiit.vehicledetect.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

public class VdActivity extends Activity implements CvCameraViewListener2 {

    private static final String    	TAG                 	= "VehicleDetection::Activity";
    public static final int        	JAVA_DETECTOR       	= 0;
    public static final int        	NATIVE_DETECTOR     	= 1;

    private MenuItem               	mItemAngle;
    private MenuItem               	mItemHeight;
    
    private Mat                    	mRgba;
    private Mat                    	mGray;
    private File                   	mCascadeFile;
    private CascadeClassifier      	mJavaDetector;
    private DetectionBasedTracker  	mNativeDetector;
    
    private int                    	mDetectorType       	= NATIVE_DETECTOR;//JAVA_DETECTOR;

    private float                  	mRelativeFaceSize   	= 0.2f;
    private int                    	mAbsoluteFaceSize   	= 0;
    
    private int 				   	mFrameNo			   	= 0;
    LinkedList<Long> 				mPreviousFrameTime 		= new LinkedList<Long>();
    private double				   	mFPS				   	= 0;	

    private CameraBridgeViewBase   	mOpenCvCameraView;
        
    final float[] 					mValuesMagnet      		= new float[3];
    final float[] 					mValuesAccel       		= new float[3];
    final float[] 					mValuesOrientation 		= new float[4];
    final float[] 					mRotationMatrix    		= new float[16];
    
    private DetectedVehiclesController mVehiclesController 	= new DetectedVehiclesController();
    private GPS			 			mGPS;				   
    private Accelerometer			mAccel;	
    
    //TESTING
    File 							myFile;
    OutputStreamWriter 				myOutWriter;
    FileOutputStream 				fOut;
    long 							lastTestOutput = 0;
   
    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this)
    {
        @Override
        public void onManagerConnected(int status) 
        {
            switch (status) 
            {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");

                    // Load native library after(!) OpenCV initialization
                    System.loadLibrary("detection_based_tracker");

                    try 
                    {
                        // load cascade file from application resources
                        InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
                        File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                        mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
                        FileOutputStream os = new FileOutputStream(mCascadeFile);

                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = is.read(buffer)) != -1) 
                        {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();

                        mJavaDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());
                        if (mJavaDetector.empty()) 
                        {
                            Log.e(TAG, "Failed to load cascade classifier");
                            mJavaDetector = null;
                        } 
                        else Log.i(TAG, "Loaded cascade classifier from " + mCascadeFile.getAbsolutePath());

                        mNativeDetector = new DetectionBasedTracker(mCascadeFile.getAbsolutePath(), 0);

                        cascadeDir.delete();

                    } 
                    catch (IOException e) 
                    {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
                    }

                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public VdActivity() 
    {
        //mDetectorName = new String[2];
        //mDetectorName[JAVA_DETECTOR] = "Java";
        //mDetectorName[NATIVE_DETECTOR] = "Native (tracking)";

        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.vehicle_detect_surface_view);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.vd_activity_surface_view);
        // register listener
        mOpenCvCameraView.setCvCameraViewListener(this);
        
        mVehiclesController = new DetectedVehiclesController();
        mGPS 				= new GPS(this);
        mAccel 				= new Accelerometer(this);
        
        if (Settings.TESTING_MODE)
        {
	        try 
	        {
	            myFile = new File("/sdcard/mysdfile.txt");
	            myFile.createNewFile();
	            fOut = new FileOutputStream(myFile);
	            myOutWriter = new OutputStreamWriter(fOut);
	        } 
	        catch (Exception e) 
	        {
	            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
	        }
        }
        
      /*  String filename = "myfile.txt";
        String string = "Hello world!";
        FileOutputStream outputStream;

        try {
          outputStream = openFileOutput(filename, Context.MODE_WORLD_READABLE);
          outputStream.write(string.getBytes());
          outputStream.close();
          Toast.makeText(getBaseContext(),
                  "Done writing internal file",
                  Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
          e.printStackTrace();
        }*/
        
        
    }
    
    // Register the event listener and sensor type.
    public void setListners(SensorManager sensorManager, SensorEventListener mEventListener)
    {
        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), 
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), 
                SensorManager.SENSOR_DELAY_NORMAL);
    }
    
    public void updateOrientation()
    {
    	Log.i(TAG, "Updating orientation");
    	Log.i(TAG, "Accel: " + mValuesAccel[0] + " " + mValuesAccel[1] + " " + mValuesAccel[2]);
    	Log.i(TAG, "Magnet: " + mValuesMagnet[0] + " " + mValuesMagnet[1] + " " + mValuesMagnet[2]);
    	SensorManager.getRotationMatrix(mRotationMatrix, null, mValuesAccel, mValuesMagnet);
    	Log.i(TAG, "Got rotation matrix: " );
    	Log.i(TAG, mRotationMatrix[0] + " " + mRotationMatrix[1] + " " + mRotationMatrix[2] );
    	Log.i(TAG, mRotationMatrix[3] + " " + mRotationMatrix[4] + " " + mRotationMatrix[5] );
    	Log.i(TAG, mRotationMatrix[6] + " " + mRotationMatrix[7] + " " + mRotationMatrix[8] );
        SensorManager.getOrientation(mRotationMatrix, mValuesOrientation);
        final String test;
        test = "Orientation results: " + mValuesOrientation[0] +" "+mValuesOrientation[1]+ " "+ mValuesOrientation[2];
        Log.i(TAG, test);
        //txt1.setText(test);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
    }

    public void onDestroy() 
    {
        super.onDestroy();
        mOpenCvCameraView.disableView();
       
        if (Settings.TESTING_MODE)
        {
        
	        try
	        {
		        myOutWriter.close();
		        fOut.close();
		        Toast.makeText(getBaseContext(),
		                "Done writing SD 'mysdfile.txt'",
		                Toast.LENGTH_SHORT).show();
	        } catch (Exception e) {
	            Toast.makeText(getBaseContext(), e.getMessage(),
	                    Toast.LENGTH_SHORT).show();
	        }
        }
    }

    public void onCameraViewStarted(int width, int height) 
    {
    	// prepare storage for frame
    	mGray = new Mat();
        mRgba = new Mat();
    }

    public void onCameraViewStopped() 
    {
    	// release storage for frame
    	mGray.release();
        mRgba.release();
    }

    
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) 
    {

        Log.i(TAG, "Processing the imeage");
        updateOrientation();
    	
    	mRgba = inputFrame.rgba();
    	mGray = inputFrame.gray();
    	
		Settings.setImageHeight(mGray.height());
    	
		//calculate FPS
		long currentTime = System.currentTimeMillis();
		if (mPreviousFrameTime.size() != 0) 
		{
			mFPS = Math.round(1000.0f / (currentTime - mPreviousFrameTime.getLast()) * mPreviousFrameTime.size());
		}
		if (mPreviousFrameTime.size() >= Settings.FRAME_TIME_MEMORY) mPreviousFrameTime.removeLast();
		mPreviousFrameTime.addFirst(currentTime);
		
		// update detected vehicles every "FRAME_FREQUENCY" frames
		if (++mFrameNo >= Settings.FRAME_FREQUENCY)
		{
			if (mAbsoluteFaceSize == 0) 
	        {
	            int height = mGray.rows();
	            if (Math.round(height * mRelativeFaceSize) > 0) 
	            {
	                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
	            }
	            mNativeDetector.setMinVehicleSize(mAbsoluteFaceSize);
	        }

	        MatOfRect vehicles = new MatOfRect();

	        if (mDetectorType == JAVA_DETECTOR) 
	        {
	            if (mJavaDetector != null)
	                mJavaDetector.detectMultiScale(mGray, vehicles, 1.1, 2, 2, // TODO: objdetect.CV_HAAR_SCALE_IMAGE
	                        new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());
	        }
	        else if (mDetectorType == NATIVE_DETECTOR) 
	        {
	            if (mNativeDetector != null)
	                mNativeDetector.detect(mGray, vehicles);
	        }
	        else 
	        {
	            Log.e(TAG, "Detection method is not selected!");
	        }

	        mVehiclesController.update(vehicles.toArray(), mGray);			
		}
		
		//printout FPS
		Core.putText(mRgba, Double.toString(mFPS) + " fps", new Point(0, 20), 2, 1, Settings.TEXT_COLOR, 1);
		
		//printout my speed
		Core.putText(mRgba, Double.toString(Math.round(mGPS.getSpeed())) + " km/h", new Point(mRgba.width() - 200, 28), 2, 1, Settings.VEHICLE_TRACKING_COLOR, 1);
		
		//printout angle
		//Core.putText(mRgba, Double.toString(Math.round(mAccel.getAngle())), new Point(mRgba.width() - 500, 28), 2, 1, Settings.VEHICLE_TRACKING_COLOR, 1);
			
		//Core.rectangle(mRgba, new Point(0, 100), new Point(mRgba.width(), 100), Settings.TEXT_COLOR, 1);
		
		// draw vehicles to output image
		for (VehicleDrawData vehicle : mVehiclesController.getVehicles())
        {
        	// draw rectangle 
        	Core.rectangle(mRgba, vehicle.getPosition().tl(), vehicle.getPosition().br(), vehicle.getColor(), 3);
        	// draw point at the bottom of vehicle
        	Core.rectangle(mRgba, new Point(0, vehicle.getVehicleBottom()), new Point(mRgba.width(), vehicle.getVehicleBottom()), Settings.TEXT_COLOR, 2);
        	
        	// print speed
        	//Log.i(TAG, "GPS: " + Double.toString(gps.getSpeed()));
        	String out = String.format( Locale.ENGLISH, "%.3g km/h", vehicle.getSpeed() + mGPS.getSpeed());
            Core.putText(mRgba, out, new Point(vehicle.getPosition().tl().x, vehicle.getPosition().br().y + 25), 3, 1, Settings.TEXT_COLOR, 2);
            
            // save speed to testing output file if test mode enabled
            if (Settings.TESTING_MODE)
            {
	            if (currentTime - lastTestOutput >= Settings.TESTING_OUTPUT_FREQUENCY)
	            {	
	            	lastTestOutput = currentTime;
		            try 
		            {
		    			myOutWriter.append(String.format( Locale.ENGLISH, "%.3g, %.3g\n", vehicle.getSpeed(), mGPS.getSpeed()));
		    		} 
		            catch (IOException e) 
		    		{
		    			e.printStackTrace();
		    		}
	            }
        	}
        }	
        
        return mRgba;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        Log.i(TAG, "called onCreateOptionsMenu");
        mItemAngle = menu.add("Kalibr�cia sklonu");
        mItemHeight = menu.add("Kalibr�cia v��ky");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
        if (item == mItemAngle)
        {	
        	setDeviceAngle();
        }    
        else if (item == mItemHeight)
        {
        	setDeviceHeight();
    	}
       
        return true;
    }
    
    private void setDeviceAngle()
    {
	    Settings.setDeviceAngle(mAccel.getAngle());
    }
    
    private void setDeviceHeight()
    {
    	
    	final Context context = this;
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle("Zadaj v��ku nad cestou [cm]");

    	// Set up the input
    	final EditText input = new EditText(this);
    	input.setInputType(InputType.TYPE_CLASS_NUMBER); 
    	builder.setView(input);

    	// Set up the buttons
    	builder.setPositiveButton("OK", new DialogInterface.OnClickListener() 
    	{ 
    	    @Override
    	    public void onClick(DialogInterface dialog, int which) 
    	    {
    	        try
    	        {
    	        	Settings.setCameraHeight(Double.valueOf(input.getText().toString()));
    	        }
    	        catch (NumberFormatException e)
    	        {
    	        	AlertDialog.Builder alert = new AlertDialog.Builder(context);
    	        	alert.setTitle("Nespr�vne zadan� vstup");
    	        	alert.show();
    	        }
    	    }
    	});
    	builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
    	{
    	    @Override
    	    public void onClick(DialogInterface dialog, int which) {
    	        dialog.cancel();
    	    }
    	});

    	builder.show();
    } 
}



/*    	
Mat mat = inputFrame.rgba();
Mat src_mat = new Mat(4,1,CvType.CV_32FC2);
Mat dst_mat = new Mat(4,1,CvType.CV_32FC2);


src_mat.put(0,0,407.0,74.0,1606.0,74.0,420.0,2589.0,1698.0,2589.0);
dst_mat.put(0,0,0.0,0.0,1600.0,0.0, 0.0,2500.0,1600.0,2500.0);
//Mat perspectiveTransform = Imgproc.getPerspectiveTransform(src_mat, dst_mat);

Mat dst=mat.clone();

List<Point> source = new ArrayList<Point>();
source.add(new Point(0, 0));
source.add(new Point(100, 0));
source.add(new Point(100, 100));
source.add(new Point(0, 100));
Mat startM = Converters.vector_Point2f_to_Mat(source);
        
List<Point> dest = new ArrayList<Point>();
dest.add(new Point(0, 0));
dest.add(new Point(50, 0));
dest.add(new Point(50, 50));
dest.add(new Point(0, 50));
Mat endM = Converters.vector_Point2f_to_Mat(dest);   

Mat outputMat = new Mat(mRgba.width(), mRgba.height(), CvType.CV_8UC4);

Mat perspectiveTransform = Imgproc.getPerspectiveTransform(startM, endM);

Log.i(TAG, perspectiveTransform.dump());

Imgproc.warpPerspective(mRgba, 
        outputMat,
        perspectiveTransform,
        new Size(mRgba.width(), mRgba.height()), 
        Imgproc.INTER_CUBIC);

//Imgproc.warpPerspective(mat, dst, perspectiveTransform, new Size(300,300));

return outputMat;
*/ 

/*SensorManager sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);        

final SensorEventListener mEventListener = new SensorEventListener() {
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        // Handle the events for which we registered
    	Log.i(TAG, "Sensor changed: " + event.sensor.getType() + ", "+ event.values[0]);
    	switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                System.arraycopy(event.values, 0, mValuesAccel, 0, 3);
                break;

            case Sensor.TYPE_MAGNETIC_FIELD:
                System.arraycopy(event.values, 0, mValuesMagnet, 0, 3);
                break;
        }
    };
};

setListners(sensorManager, mEventListener);

Sensor magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);


if (sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null){
	// Success! There's a magnetometer.
	Log.i(TAG, "There's a magnetometer");
  }
else {
  // Failure! No magnetometer.
	Log.i(TAG, "Failure! No magnetometer");
  }

if (sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null){
	Log.i(TAG, "There's a gyroscope");
  }
else {
	Log.i(TAG, "Failure! No gyroscope");
  }

if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null){
	Log.i(TAG, "There's a ACCELEROMETER");
  }
else {
	Log.i(TAG, "Failure! No ACCELEROMETER");
  }

if (sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY) != null){
	Log.i(TAG, "There's a GRAVITY sensor");
  }
else {
	Log.i(TAG, "Failure! No GRAVITY sensor");
  }

List<Sensor> deviceSensors = sensorManager.getSensorList(Sensor.TYPE_ALL);
for (Sensor sensor : deviceSensors) {
	Log.i(TAG, sensor.getName());
} 

*/
