package sk.fiit.vehicledetect;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;

import org.opencv.core.Point;
import org.opencv.core.Rect;

import sk.fiit.general.Settings;

import android.util.Log;

public class DetectedVehicle {
	private static final String    TAG = "OCVSample::DetectedVehicle";
	
	private int 	age; 
	private int 	speedRefresh;
	private double 	speed;
	
	private LinkedList<Position> positions = new LinkedList<Position>();
	
	/**
	 * Constructor
	 * 
	 * @param currentPosition
	 * @param currentTime
	 */
	public DetectedVehicle(Rect currentPosition, int vehicleBottom, long currentTime)
	{
		age = 0;
		speedRefresh = Settings.SPEED_REFRESH_TRASHOLD - 1;
		speed = 0.0;
		
		this.update(currentPosition, vehicleBottom, currentTime);
	}
	
	
	/**
	 * Public method to update position of detected vehicle
	 *  
	 * @param currentPosition
	 * @param currentTime
	 */
	public void update(Rect currentPosition, int vehicleBottom, long currentTime)
	{
		// delete last position if memory limit exceeded
		if (positions.size() >= Settings.POSITIONS_MEMORY_SIZE)
		{
			positions.removeLast();
		}
		
		// remember the new position
		if (positions.size() > 0) positions.addFirst(new Position(averageRect(currentPosition, positions.getFirst().getPos()), vehicleBottom, currentTime));
		else positions.addFirst(new Position(currentPosition, vehicleBottom, currentTime));
		
		Log.i(TAG, "# positions:" + Integer.toString(positions.size()));
		
		// set age to zero
		age = 0;
		speedRefresh++;
	}
	
	/**
	 * Public method to compute distance from detected vehicle. 
	 * Parameter positionNo specifies which position will used for computations.
	 *   
	 * @param positionNo
	 * @return
	 */
	public double computeDistance(int positionNo){
		// compute exact angle
		double radians = Math.toRadians(90 - Settings.DEVICE_ANGLE + Settings.HORIZONTAL_FIELD_OF_VIEW / 2.0f  - (new Double(positions.get(positionNo).getVehicleBottom()) /*getPos().br().y*/ / Settings.IMAGE_HEIGHT) * Settings.HORIZONTAL_FIELD_OF_VIEW);
        // compute distance 
		double distance = Math.tan(radians) * Settings.HEIGHT_OF_CAMERA_ABOVE_ROAD; 
		// convert to meters
		
		Log.e(TAG, "distance:"  + Double.toString(distance / 100.0f));
		return distance / 100.0f;
	}
	
	/**
	 * Public method to compute speed of vehicle. 
	 * Speed is computed from differences in distances and time between first and last known position.
	 *
	 * @return
	 */
	public double computeSpeed()
	{
		this.speedRefresh = 0;
		
		// compute distances 
		double firstDistance = computeDistance(0);
		double lastDistance = computeDistance(positions.size() - 1);
		Log.i(TAG, "get speed distances:" + Double.toString(firstDistance) + ", " + Double.toString(lastDistance));
		
		// get time difference in seconds
		double deltaTime = ((positions.getFirst().getTime() - positions.getLast().getTime()) / 1000.0f);
		if (deltaTime == 0) return 0.0f;
		Log.i(TAG, "get speed delta times:" + Long.toString(positions.getFirst().getTime()) + ", " + Long.toString(positions.getLast().getTime()));		
		
		// compute speed and convert from m/s to km//h
		return round((firstDistance - lastDistance) / deltaTime * 3.6, 2);
		 
	}
	
	public double getSpeed()
	{
		if (this.speedRefresh >= Settings.SPEED_REFRESH_TRASHOLD) this.speed = computeSpeed();
		
		return this.speed;
	}
	
	
	/**
	 * @param currentPosition
	 * @param imageHeight
	 * @return
	 */
	public boolean isTheSameVehicle(Rect currentPosition, int imageHeight)
	{
		// compute middles of last known position and new one
		Point middleOld = rectMiddle(positions.getFirst().getPos());
		Point middleNew = rectMiddle(currentPosition);
		Log.i(TAG, "is the same:" + Double.toString(Math.abs(middleOld.x - middleNew.x) / imageHeight) + ", " + Double.toString(Math.abs(middleOld.y - middleNew.y) / imageHeight));
    	if ((Math.abs(middleOld.x - middleNew.x) / imageHeight < Settings.DISTANCE_TRASHOLD) && (Math.abs(middleOld.y - middleNew.y) / imageHeight < Settings.DISTANCE_TRASHOLD)) return true;
		return false;
	}
	
	public boolean isOld()
	{
		this.age++;
		return  this.age >= Settings.POSITION_IS_OLD ? true : false;
	} 
	
	public boolean isNew()
	{
		return this.positions.size() > 1 ? false: true;
	}
	
	public Position getLastKnownPosition()
	{
		return positions.getFirst();
	}
	
	private Point rectMiddle(Rect rect)
    {
    	return new Point((int) Math.round((rect.tl().x + rect.br().x) / 2.0f), (int) Math.round((rect.tl().y + rect.br().y) / 2.0f));
    }
	
	private static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	private Rect averageRect(Rect newR, Rect oldR)
    {
    	Point newMiddle = rectMiddle(newR); 
    	int newWidth = (int) Math.round((oldR.width * 0.2f + newR.width * 0.8f));
    	int newHeight = (int) Math.round((oldR.height * 0.2f + newR.height * 0.8f));
    	return new Rect((int) Math.round(newMiddle.x - (newWidth / 2.0f)), (int) Math.round(newMiddle.y - (newHeight / 2.0f)), newWidth, newHeight); 
    }
	 
}
