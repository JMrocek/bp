package sk.fiit.vehicledetect;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;

public class GSensor extends Activity implements SensorEventListener  {
	private float[] mGravity;
    private float[] mMagnetic;
    
    private float orientation = 0;
   
    private static final String TAG = "G sensor";
    
    private static SensorManager sensorManager;
    private Sensor sensor;
    
    public float getOrientation()
    {
    	return orientation;
    }
    
    
    private float getDirection() 
    {
           	
    	float[] temp = new float[9];
        float[] R = new float[9];
        //Load rotation matrix into R
        SensorManager.getRotationMatrix(temp, null,
                mGravity, mMagnetic);
       
        //Remap to camera's point-of-view
        SensorManager.remapCoordinateSystem(temp,
                SensorManager.AXIS_X,
                SensorManager.AXIS_Z, R);
       
        //Return the orientation values
        float[] values = new float[3];
        SensorManager.getOrientation(R, values);
       
        //Convert to degrees
        for (int i=0; i < values.length; i++) {
            Double degrees = (values[i] * 180) / Math.PI;
            values[i] = degrees.floatValue();
        }

        return values[0];
    }
   
    
    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      
      Log.i("Compass MainActivity", "Creating ORIENTATION Sensor");
      
      sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
      sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
      if (sensor != null) {
    	  sensorManager.registerListener(this/*mySensorEventListener*/, sensor,
            SensorManager.SENSOR_DELAY_NORMAL);
        Log.i("Compass MainActivity", "Registerered for ORIENTATION Sensor");
      } else {
        Log.e("Compass MainActivity", "Registerered for ORIENTATION Sensor");
        /*Toast.makeText(this, "ORIENTATION Sensor not found",
            Toast.LENGTH_LONG).show();*/
        finish();
      }
    }
    
    @Override
    public void onSensorChanged(SensorEvent event) {
        
    	Log.i(TAG, "Sensor event sdf gsdf gsdf g	");
    	
    	switch(event.sensor.getType()) {
               
        case Sensor.TYPE_ACCELEROMETER:
            mGravity = event.values.clone();
            break;
        case Sensor.TYPE_MAGNETIC_FIELD:
            mMagnetic = event.values.clone();
            break;
        default:
            return;
        }
        if(mGravity != null && mMagnetic != null) {
        	orientation = getDirection();
        }
    }

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onResume() {
	    super.onResume();
	    sensorManager.registerListener(this,sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),SensorManager.SENSOR_DELAY_GAME);
	}

	@Override
	protected void onPause() {
	    super.onPause();
	    sensorManager.unregisterListener(this);
	}

}
