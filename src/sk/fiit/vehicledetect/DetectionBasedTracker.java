package sk.fiit.vehicledetect;

import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;

public class DetectionBasedTracker
{
    public DetectionBasedTracker(String cascadeName, int minVehiclesSize) {
        mNativeObj = nativeCreateObject(cascadeName, minVehiclesSize);
    }

    public void start() {
        nativeStart(mNativeObj);
    }

    public void stop() {
        nativeStop(mNativeObj);
    }

    public void setMinVehicleSize(int size) {
        nativeSetVehicleSize(mNativeObj, size);
    }

    public void detect(Mat imageGray, MatOfRect vehicles) {
        nativeDetect(mNativeObj, imageGray.getNativeObjAddr(), vehicles.getNativeObjAddr());
    }

    public void release() {
        nativeDestroyObject(mNativeObj);
        mNativeObj = 0;
    }

    private long mNativeObj = 0;

    private static native long nativeCreateObject(String cascadeName, int minVehicleSize);
    private static native void nativeDestroyObject(long thiz);
    private static native void nativeStart(long thiz);
    private static native void nativeStop(long thiz);
    private static native void nativeSetVehicleSize(long thiz, int size);
    private static native void nativeDetect(long thiz, long inputImage, long vehicles);
}
