package sk.fiit.vehicledetect;

import org.opencv.core.Rect;

public class Position {
	private Rect pos;
	private int vehicleBottom;
	private long time;
	
	public Position(Rect currentPosition, int vehicleBottom, long currentTime)
	{
		this.pos = currentPosition;
		this.time = currentTime;
		this.vehicleBottom = vehicleBottom;
		
	}
	
	public Rect getPos() {
		return pos;
	}
	public void setPos(Rect pos) {
		this.pos = pos;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public int getVehicleBottom()
	{
		return vehicleBottom;
	}
	
	
	
}
