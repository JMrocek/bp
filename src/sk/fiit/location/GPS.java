package sk.fiit.location;

import sk.fiit.general.Settings;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class GPS
{ 
    private static final String 	TAG 		= "VehicleDetection::GPS";
    private double 					speed 		= 0; 
	private long 					captured 	= 0;
	private LocationManager 		manager; 
    private LocationListener 		listener; 
    
    
    public GPS(Context context)
    {
        manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        
        // check if device has GPS 
        if (manager.getAllProviders().contains(LocationManager.GPS_PROVIDER))
        {    
        	// create listener 
        	listener = new GPSActionListener(); 
        	// register listener
        	manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, listener);
        	
        	Log.i(TAG, "GPS initialized");
        }
        else
        {
        	Log.e(TAG, "GPS not found");
        }	
        
  	
    }

    public double getSpeed()
    {
    	if (captured == 0) return 0;
    	
    	// check if value is not old
    	long currentTime = System.currentTimeMillis();
    	if (currentTime - captured > Settings.MAX_GPS_SIGNAL_AGE) return 0; 
		
    	return this.speed;
    }
    
    
    private class GPSActionListener implements LocationListener 
    { 
    	@Override 
        public void onLocationChanged(Location location) 
        { 
        	if(location!=null) 
        	{ 
        		if(location.hasSpeed())
        		{ 
        			speed = location.getSpeed() * 3.6f; // convert from m/s to km/h 
        			captured = System.currentTimeMillis();
                } 
            } 
        } 

        @Override 
        public void onProviderDisabled(String provider) { } 

        @Override 
        public void onProviderEnabled(String provider) { } 

        @Override 
        public void onStatusChanged(String provider, int status, Bundle extras) { } 
    } 
}



