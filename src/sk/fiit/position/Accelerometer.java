package sk.fiit.position;

import sk.fiit.general.Settings;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class Accelerometer {

	private static final String 			TAG 		= "VehicleDetection::Accelerometer";
    private double 							angle 		= Settings.ANGLE_NOT_SET; 
    private int								counter 	= 0;
    private double							sum			= 0;
	private SensorManager	 				manager;
	private Sensor							sensor;
    private AccelerometerActionListener 	listener; 
   
    
    
    public Accelerometer(Context context)
    {
    	manager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
    	if (manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null){
    		Log.i(TAG, "There's a ACCELEROMETER");
    		
    		sensor = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            listener = new AccelerometerActionListener(); 
            manager.registerListener(listener, sensor , SensorManager.SENSOR_DELAY_NORMAL);
        	
    	}
        else
        {
        	Log.e(TAG, "ACCELEROMETER not found");
        }	
    }

    public double getAngle()
    {
    	while (angle == Settings.ANGLE_NOT_SET)
			try 
    		{
				Thread.sleep(100);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
    	
    	return angle;
    }
    
    
    private class AccelerometerActionListener implements SensorEventListener 
    {
		@Override
		public void onSensorChanged(SensorEvent event) 
		{
			Sensor mySensor = event.sensor;
			 
		    if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) 
		    {
		    	float x = event.values[0];
		    	if (counter >= Settings.ANGLE_CALIBRATION_COUNT)
		    	{
		    		angle = sum / Settings.ANGLE_CALIBRATION_COUNT;
		    		sum = 0;
		    		counter = 0;
		    	} 
		    	
		    	double currentAngle = 90 - (x / Settings.GRAVITY_ACCELERATION * 90.0f);//90 - Math.toDegrees(Math.asin(x / Settings.GRAVITY_ACCELERATION));//Math.asin(x / Settings.GRAVITY_ACCELERATION);
		    	if (currentAngle == Double.NaN) currentAngle = 0;
		    	sum += currentAngle;
		    	counter++;
		    	//angle = 90 - (x / Settings.GRAVITY_ACCELERATION * 90.0f);
		    }
			
		}
		
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) { }

    	
    } 
}



